/*
old
0.149 sec
*/
SELECT count(*) AS AGGREGATE
FROM (SELECT *
      FROM `colli`
      WHERE (`booking_id` IS NULL
          OR EXISTS
                 (SELECT *
                  FROM `bookings`
                  WHERE `colli`.`booking_id` = `bookings`.`id`
                    AND `status` <> 10))
        AND date(`received_at`) >= '2022-04-25'
        AND date(`received_at`) <= '2023-04-25'
      GROUP BY `client_id`) AS `aggregate_table`;

/*
new
0.092 sec
*/
SELECT COUNT(DISTINCT c.client_id) AS AGGREGATE
FROM colli c
         LEFT JOIN bookings b ON c.booking_id = b.id
WHERE (c.booking_id IS NULL
    OR b.status <> 10)
  AND c.received_at BETWEEN '2022-04-25' AND '2023-04-25';