/*
old
0.382
*/

SELECT *
FROM `bookings`
WHERE `deleted_at` IS NULL
ORDER BY (SELECT name
          FROM addresses
          WHERE addresses.id = bookings.dropoff_address) ASC
LIMIT 10000 OFFSET 0;


/*
new
0.308 sec
*/

SELECT bookings.*,
       addresses.name AS address_name
FROM bookings
         INNER JOIN addresses ON addresses.id = bookings.dropoff_address
WHERE bookings.deleted_at IS NULL
ORDER BY addresses.name ASC
LIMIT 10000 OFFSET 0;