/*
old
0.004 sec
*/
SELECT *
FROM `agreements`
ORDER BY (SELECT name
          FROM clients
          WHERE clients.id = agreements.client_id);


/*
new
0.002 sec
*/

SELECT agreements.*,
       clients.name AS client_name
FROM agreements AS agreements
         INNER JOIN clients AS clients ON clients.id = agreements.client_id
ORDER BY clients.name;