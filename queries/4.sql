/*
old
0.361 sec
*/
SELECT *
FROM `bookings`
WHERE `deleted_at` IS NULL
  AND (`id` LIKE '%10000%'
    OR concat("B-", id) LIKE '%10000%'
    OR `reference` LIKE '%10000%'
    OR EXISTS
           (SELECT *
            FROM `clients`
            WHERE `bookings`.`client_id` = `clients`.`id`
              AND `name` LIKE '%10000%')
    OR EXISTS
           (SELECT *
            FROM `addresses`
            WHERE `bookings`.`pickup_address` = `addresses`.`id`
              AND (`name` LIKE '%10000%'
                OR `street` LIKE '%10000%'
                OR `zip` LIKE '%10000%'
                OR `city` LIKE '%10000%'))
    OR EXISTS
           (SELECT *
            FROM `addresses`
            WHERE `bookings`.`dropoff_address` = `addresses`.`id`
              AND (`name` LIKE '%10000%'
                OR `street` LIKE '%10000%'
                OR `zip` LIKE '%10000%'
                OR `city` LIKE '%10000%'))
    OR EXISTS
           (SELECT *
            FROM `trips`
            WHERE `bookings`.`id` = `trips`.`booking_id`
              AND EXISTS
                (SELECT *
                 FROM `addresses`
                 WHERE `trips`.`trip_address` = `addresses`.`id`
                   AND `phone` LIKE '%10000%'))
    OR EXISTS
           (SELECT *
            FROM `trips`
            WHERE `bookings`.`id` = `trips`.`booking_id`
              AND (`id` LIKE '%10000%'
                OR concat("T-", id) LIKE '%10000%'))
    OR EXISTS
           (SELECT *
            FROM `colli`
            WHERE `bookings`.`id` = `colli`.`booking_id`
              AND `reference` LIKE '%10000%'))
ORDER BY `id` DESC
LIMIT 10001 OFFSET 0;

/*
new
slower
*/
SELECT b.*
FROM bookings AS b
         LEFT JOIN clients AS c ON b.client_id = c.id
         LEFT JOIN addresses AS pa ON b.pickup_address = pa.id
         LEFT JOIN addresses AS da ON b.dropoff_address = da.id
         LEFT JOIN trips AS t ON b.id = t.booking_id
         LEFT JOIN addresses AS ta ON t.trip_address = ta.id
         LEFT JOIN colli AS co ON b.id = co.booking_id
WHERE b.deleted_at IS NULL
  AND (b.id LIKE '%10000%'
    OR concat("B-", b.id) LIKE '%10000%'
    OR b.reference LIKE '%10000%'
    OR c.name LIKE '%10000%'
    OR pa.name LIKE '%10000%'
    OR pa.street LIKE '%10000%'
    OR pa.zip LIKE '%10000%'
    OR pa.city LIKE '%10000%'
    OR da.name LIKE '%10000%'
    OR da.street LIKE '%10000%'
    OR da.zip LIKE '%10000%'
    OR da.city LIKE '%10000%'
    OR ta.phone LIKE '%10000%'
    OR t.id LIKE '%10000%'
    OR concat("T-", t.id) LIKE '%10000%'
    OR co.reference LIKE '%10000%')
ORDER BY b.id DESC
LIMIT 10000 OFFSET 0;
