CREATE VIEW colliBatchCreated
AS
SELECT colli_batch, colli.created_at
FROM colli
         LEFT JOIN bookings ON colli.booking_id = bookings.id
         LEFT JOIN locations ON colli.location_id = locations.id
WHERE (colli.booking_id IS NULL OR bookings.status <> 10)
  AND locations.warehouse_id = 1
  AND colli_packing_status NOT IN (2, 3)
  AND colli.status <> 8;