1.

/*
old

*/

SELECT COUNT(*) AS colli_count
FROM (
         SELECT *
         FROM colli
         WHERE (
                 booking_id IS NULL
                 OR EXISTS(
                         SELECT *
                         FROM bookings
                         WHERE colli.booking_id = bookings.id
                           AND status <> 10
                     )
             )
           AND EXISTS(
                 SELECT *
                 FROM locations
                 WHERE colli.location_id = locations.id
                   AND warehouse_id = 1
             )
           AND colli_packing_status NOT IN (2, 3)
           AND status <> 8
         GROUP BY colli_batch
     ) AS count;